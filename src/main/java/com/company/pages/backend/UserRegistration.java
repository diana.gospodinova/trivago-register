package com.company.pages.backend;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.company.tests.backend.UserTests.browser;

public class UserRegistration {

    public void browseToPage(String url) {
        browser.getDriver().get(url);
    }

    public void register(String userEmail, String password) throws InterruptedException {
        WebElement registerButton = browser.getDriver().findElement(By.xpath("//span[contains(text(),'Регистрация')]"));
        registerButton.click();

        Thread.sleep(5000);

        WebElement emailField = (new WebDriverWait(browser.getDriver(), 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("register_email")));
        emailField.sendKeys(userEmail);


        WebElement passwordField = browser.getDriver().findElement(By.id("register_password"));
        passwordField.sendKeys(password);

        WebElement loginButton = browser.getDriver().findElement(By.id("register_email_submit"));
        loginButton.click();
    }

    public void assertUserIsRegisteredAndLogged(String userEmail) {
        WebElement validationElement = (new WebDriverWait(browser.getDriver(), 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@data-di-mask='true']")));
        String observedEmail = validationElement.getText();

        Assert.assertEquals("The expected user email " + userEmail + " did not match", userEmail, observedEmail);
    }
}
