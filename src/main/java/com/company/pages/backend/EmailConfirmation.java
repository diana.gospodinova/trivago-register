package com.company.pages.backend;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.company.tests.backend.UserTests.browser;

public class EmailConfirmation {

    public void browseToPage(String url) {
        browser.getDriver().get(url);
    }

    public void confirmEmail(String userEmail) throws InterruptedException {
        WebElement emailField = browser.getDriver().findElement(By.id("inboxfield"));
        emailField.sendKeys(userEmail);

        WebElement searchButton = browser.getDriver().findElement(By.id("go_inbox1"));
        searchButton.click();

        browser.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement confirmationEmail = browser.getDriver().findElement(By.xpath("//a[contains(text(),'Активирайте Вашия trivago профил')]"));
        confirmationEmail.click();

        JavascriptExecutor jsx = browser.getDriver();
        jsx.executeScript("window.scrollBy(0,450)", "");

        browser.getDriver().switchTo().frame("msg_body");
        Thread.sleep(5000);

        WebElement confirmationButton = browser.getDriver().findElement(By.xpath("//a[contains(text(),'Активиране на акаунт')]"));
        confirmationButton.click();
    }

    public void assertEmailConfirmationMessage(String message) {
        Set<String> tab_handles = browser.getDriver().getWindowHandles();
        int number_of_tabs = tab_handles.size();
        int new_tab_index = number_of_tabs-1;
        browser.getDriver().switchTo().window(tab_handles.toArray()[new_tab_index].toString());

        WebElement validationElement = (new WebDriverWait(browser.getDriver(), 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[contains(text(),'Имейл адресът Ви бе потвърден успешно. Можете да затворите тази страница.')]")));
        String observedEmail = validationElement.getText();

        Assert.assertEquals("The expected message " + message + " did not match", message, observedEmail);
    }
}
