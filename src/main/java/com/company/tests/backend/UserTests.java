package com.company.tests.backend;

import com.company.core.functionality.ChromeBrowser;
import com.company.pages.backend.EmailConfirmation;
import com.company.pages.backend.UserRegistration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTests {
    public static ChromeBrowser browser;

    @Before
    public void setUp() {
        browser = new ChromeBrowser();
        browser.setUp();
    }

    @Test
    public void successfulUserRegistration() throws InterruptedException {
        // Arrange
        UserRegistration userLogin = new UserRegistration();
        userLogin.browseToPage("https://www.trivago.bg/");

        // Act
        userLogin.register("trivago1111@mailinator.com", "123456");

        // Assert
        userLogin.assertUserIsRegisteredAndLogged("trivago1111@mailinator.com");
    }

    @Test
    public void successfulEmailConfirmation() throws InterruptedException {
        // Arrange
        EmailConfirmation emailConfirmation = new EmailConfirmation();
        emailConfirmation.browseToPage("https://www.mailinator.com/");

        // Act
        emailConfirmation.confirmEmail("trivago1111@mailinator.com");

        // Assert
        emailConfirmation.assertEmailConfirmationMessage("Имейл адресът Ви бе потвърден успешно. Можете да затворите тази страница.");
    }

    @After
    public void closeBrowser() {
        browser.quit();
    }
}
