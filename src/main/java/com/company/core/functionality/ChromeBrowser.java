package com.company.core.functionality;

import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static com.company.tests.backend.UserTests.browser;

public class ChromeBrowser implements Browser {
    private ChromeDriver driver;

    public ChromeBrowser() {
        this.driver = new ChromeDriver();
    }

    public ChromeDriver getDriver() {
        return driver;
    }

    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Didi\\Desktop\\TrivagoAll\\Trivago\\chromedriver.exe");
        driver = browser.getDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void quit() {
        driver.quit();
    }
}
