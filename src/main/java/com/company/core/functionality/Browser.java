package com.company.core.functionality;

public interface Browser {
    void setUp();

    void quit();
}
